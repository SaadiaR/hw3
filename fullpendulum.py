# ----------------------------------------------------
# File: fullpendulum.py
# ----------------------------------------------------
# Author: Sayyeda Saadia Razvi, Sarah Elias, Jake Lewis, BitBucket handle (SaadiaR)
# ----------------------------------------------------
# Plaftorm: Windows 7
# Environment: Python 2.7 
# Libaries: NumPy 1.8.1
#           MatPlotLib 1.3.1
#           SciPy 0.14.0
#           Tkinter $Revision: 81008 $
# ----------------------------------------------------
# Description: 
# This program uses tkinter to create a GUI for viewing a single or double pendulum simulation
# The GUI allows the user to change parameters using sliders, drop down menus, text boxes, etc
# The program uses object oriented programming to accomplish this 
# ---------------------------------------------------

import Tkinter as tk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import matplotlib.animation as animation

# ==================================================================================

# main menu class that calls GUI for single and double pendulum
class MainMenu(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
     
        self.master = master
        QUIT = tk.Button(master, text="Quit", command=self.quit)
        QUIT.pack()
    
        self.links = tk.StringVar(master)
        self.links.set("Single Pendulum") # initial value
        
        # drop down menu for selecting pendulum 1 or 2
        option = tk.OptionMenu(master, self.links, "Single Pendulum", "Double Pendulum")
        option.pack()
        
        # button for running the selected pendulum
        go = tk.Button(master, text="Go!", command=self.choice)
        go.pack()
           
    def choice(self):
        if self.links.get() == "Single Pendulum":
            print "Single Pendulum selected"
            self.newWindow = tk.Toplevel(self.master)
            self.newWindow.wm_title("Single Pendulum")
            self.app = singlependulumGUI(self.newWindow)

        if self.links.get() == "Double Pendulum":
            print "Double Pendulum selected"
            self.newWindow = tk.Toplevel(self.master)
            self.newWindow.wm_title("Double Pendulum")
            self.app = doublependulumGUI(self.newWindow)

# =================================================================================================

#Class for single and double pendulums. This is the physics.
class Pendulum():
    def __init__(self, link = 1):
        self.g = 9.81 #m/s**2
        self.link = link # number of links in the pendulum (1 or 2)
        # self.initial_conditions() ## These two lines have to be commented out 
        # self.ODE_solver() ## when being used with the GUI file
        
    def initial_conditions(self):
        self.dt = 0.05 # time interval for ODE solver
        self.t = np.arange(0.0, 20, self.dt) # list of times for ODE solver
        if self.link == 1: # for a single pendulum
            self.mass = 1
            self.length = 5
            self.theta = 90
            self.angvel = 0
        elif self.link ==2: # for a double pendulum
            self.mass1 = 1
            self.length1 = 5
            self.theta1 = 120
            self.angvel1 = 0
            self.mass2 = 1
            self.length2 = 5
            self.theta2 = 120
            self.angvel2 = 0
    
    # creates a list of the derivatives of the state vector
    # i.e. (theta_dot1, theta_double_dot1, theta_dot2, theta_double_dot2)
    def derivative_vector(self, state, t):

        if self.link == 1: #for a single pendulum
            dydx = np.zeros_like(state)
            dydx[0] = state[1] #theta_dot1 aka angvel1
            dydx[1] = -(2*self.g/self.length)*np.sin(state[0]) #theta_double_dot1

        if self.link == 2: #for a double pendulum
            dydx = np.zeros_like(state)
            
            dydx[0] = state[1]
    
            del_ = state[2]-state[0]
            den1 = (self.mass1+self.mass2)*self.length1 - self.mass2*self.length1*np.cos(del_)*np.cos(del_)
            dydx[1] = (self.mass2*self.length1*state[1]*state[1]*np.sin(del_)*np.cos(del_)
                   + self.mass2*self.g*np.sin(state[2])*np.cos(del_) + self.mass2*self.length2*state[3]*state[3]*np.sin(del_)
                   - (self.mass1+self.mass2)*self.g*np.sin(state[0]))/den1
    
            dydx[2] = state[3]
    
            den2 = (self.length2/self.length1)*den1
            dydx[3] = (-self.mass2*self.length2*state[3]*state[3]*np.sin(del_)*np.cos(del_)
                   + (self.mass1+self.mass2)*self.g*np.sin(state[0])*np.cos(del_)
                   - (self.mass1+self.mass2)*self.length1*state[1]*state[1]*np.sin(del_)
                   - (self.mass1+self.mass2)*self.g*np.sin(state[2]))/den2
        return dydx
    
    # Uses ODE solver to take the initial conditions and then use the derivatives
    # to update the state vector for the times specified.
    def ODE_solver(self):

        if self.link == 1: #for a single pendulum
            state = np.array([self.theta, self.angvel])*np.pi/180 #initial state vector
            self.solution = integrate.odeint(self.derivative_vector, state, self.t)

            self.angle = self.solution[:,0] ## list of angles over time
            self.angularvelocity = self.solution[:,1] ##list of angular velocities over time
            self.x = self.length*np.sin(self.solution[:,0]) #x position
            self.y = -self.length*np.cos(self.solution[:,0]) #y position

            return self.x, self.y

        elif self.link == 2: #for a double pendulum
            state = np.array([self.theta1, self.angvel1, self.theta2, self.angvel2])*np.pi/180
            self.solution = integrate.odeint(self.derivative_vector, state, self.t)

            self.angle1 = self.solution[:,0]
            self.angularvelocity1 = self.solution[:,1]
            self.angle2 = self.solution[:,2]
            self.angularvelocity2 = self.solution[:,3]
            
            self.x1 = self.length1*np.sin(self.solution[:,0])
            self.x2 = self.length2*np.sin(self.solution[:,2]) + self.x1
            self.y1 = -self.length1*np.cos(self.solution[:,0])
            self.y2 = -self.length2*np.cos(self.solution[:,2]) + self.y1
            return self.x1, self.x2, self.y1, self.y2
            
# ===================================================================================================

#Class for the GUI for a single pendulum
class singlependulumGUI(tk.Frame):
    
    Pen1 = Pendulum(1)  # creating Pen1 object as instance of Pendulum class with one link
    
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)

        self.grid()
        self.pack(side="top", fill="both", expand=True)
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.fig = plt.figure()
        self.Create_Widgets()
        self.pack()

    def Create_Widgets(self):
        # -----------------------------------------------------------------------------------------
        # sliders
        # mass
        self.label_Mass = tk.Label(self,text="Mass").grid(column=1,row=2)
        self.Mass = tk.Scale(self, from_=0.01, to=10.00, orient=tk.HORIZONTAL, resolution=0.01)
        self.Mass.grid(column=1, row = 1)
        self.Mass.set(1.0)
        # length
        self.label_Length = tk.Label(self,text="Length").grid(column=2,row=2)
        self.Length = tk.Scale(self, from_=0.01, to=10.00, orient=tk.HORIZONTAL, resolution=0.01)
        self.Length.grid(column=2, row = 1)
        self.Length.set(5.0)
        # theta
        self.label_Theta = tk.Label(self,text="Angle").grid(column=3,row=2)
        self.Theta = tk.Scale(self, from_=0.00, to=360.00, orient=tk.HORIZONTAL, resolution=1.0)
        self.Theta.grid(column=3, row = 1)
        self.Theta.set(90)
        # angular velocity
        self.label_Angvel = tk.Label(self,text="Angular Velocity").grid(column=4,row=2)
        self.Angvel = tk.Scale(self, from_=0.00, to=10.00, orient=tk.HORIZONTAL, resolution=0.01)
        self.Angvel.grid(column=4, row = 1)
        self.Angvel.set(0)

        # --------------------------------------------------------------------------------------------------------    
        #buttons
        self.ani_button = tk.Button(self, text="Animate!", command=self.Anim_Plot).grid(column=1,row=5)
        self.plot = tk.Button(self, text="Position Plot", command=self.All_Plot).grid(column=2,row=5)
        # self.quit = tk.Button(self, text="Quit", command=self.quit).grid(column=3,row=5)

        # drop down menu for time plots
        self.Parameter = tk.StringVar()
        self.Parameter.set("Angle")
        self.option = tk.OptionMenu(self, self.Parameter, "Angle", "Angular Velocity")
        self.option.grid(column=5, row=1)
        go = tk.Button(self, text="Plot Parameter", command = self.IndividualPlot).grid(column=5, row=2)

        # ------------------------------------------------------------------------------------------------------
        # text boxes
        self.label_tinit = tk.Label(self, text="time = ").grid(column=1,row=3)
        self.label_tinit = tk.Label(self, text="dt = ").grid(column=3,row=3)
                        
        self.time = tk.StringVar()
        entry = tk.Entry(self,textvariable=self.time).grid(column=2,row=3)
        self.time.set("1.0")
        
        self.dt_entry = tk.StringVar()
        entry = tk.Entry(self,textvariable=self.dt_entry).grid(column=4,row=3)
        self.dt_entry.set("0.05")

    # ------------------------------------------------------------------------------------------

    # Uses widgets to set initial conditions in the Pendulum class   
    def Initial_Conditions(self):
        self.Pen1.dt = float(self.dt_entry.get())           # dt, time interval
        tt = float(self.time.get())                         # time limit
        self.Pen1.t = (np.arange(0.0, tt, self.Pen1.dt))    # time as list
        self.Pen1.mass = (self.Mass.get())                  # mass
        self.Pen1.length = (self.Length.get())              # length
        self.Pen1.theta = (self.Theta.get())                # theta
        self.Pen1.angvel = (self.Angvel.get())              # angular veloctiy
        
    # Creates an animation of the pendulum (useing solution from pendulum class which does math)
    def Anim_Plot(self):
        self.fig.clear()

        self.Initial_Conditions()                # call initial condition function to set the initial condition
        self.x, self.y = self.Pen1.ODE_solver()  # calls pendulum class's ODE solver to find x and y position

        self.canvas = FigureCanvasTkAgg(self.fig, master=self)  # creates canvas for placing animation in 
        self.canvas.get_tk_widget().grid(column=1, row= 10, columnspan=10)

        # formatting:
        ax = self.fig.add_subplot(111, autoscale_on=False, xlim=(-20, 20), ylim=(-20, 20))
        ax.grid()

        self.line, = ax.plot([], [], 'o-', lw=2)
        self.time_template = 'time = %.1fs'
        self.time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)

        # generate animation        
        self.ani = animation.FuncAnimation(self.fig, self.animate, np.arange(1, len(self.x)),
                                      interval=25, blit=False, init_func=self.init)
        # self.ani.save("Pendulum1.mp4")
        self.canvas.show()
        
    def init(self):
        self.line.set_data([], [])
        self.time_text.set_text('')
        return self.line, self.time_text

    def animate(self,i):
        thisx = [0, self.x[i]]
        thisy = [0, self.y[i]]
        self.line.set_data(thisx, thisy)
        self.time_text.set_text(self.time_template%(i*self.Pen1.dt))
        return self.line, self.time_text

    # ---------------------------------------------------------------

    # Plots the x,y position of the mass with time
    def All_Plot(self):
        fig2 = plt.figure(figsize = (5,5))

        plt.plot(self.Pen1.t, self.x, label="x")
        plt.plot(self.Pen1.t, self.y, label="y")

        legend = plt.legend(loc='upper right')

        plt.title("Postion vs Time")
        plt.xlabel("Time [s]")
        plt.ylabel("Position")
        self.canvas = FigureCanvasTkAgg(fig2, master=self)
        self.canvas.get_tk_widget().grid(column=11, row= 10, columnspan=10)
        self.canvas.show()

    # Plots the angle and angular velocity of the pendulum with time
    # Parameter can be chosen from drop down menu 
    def IndividualPlot(self):
        fig3 = plt.figure(figsize = (5,5))
        fig3.clear()

        if self.Parameter.get() == "Angle":
            plt.plot( self.Pen1.t, self.Pen1.angle)
            plt.ylabel("Angle [rad]")
           
        elif self.Parameter.get() == "Angular Velocity":
            plt.plot( self.Pen1.t, self.Pen1.angularvelocity)
            plt.ylabel("Angular Velocity [rad/s]")
        
        string = "%s vs Time" % self.Parameter.get()    # labelling the graph
        plt.title(string)
        plt.xlabel("Time [s]")
        self.canvas = FigureCanvasTkAgg(fig3, master=self)
        self.canvas.get_tk_widget().grid(column=21, row= 10, columnspan=10)
        self.canvas.show()

# ============================================================================================
        
# Class for double pendulum GUI
class doublependulumGUI(tk.Frame):
    
    Pen2 = Pendulum(2) # creating Pen2 object as an instance of Pendulum class with 2 links

    def __init__(self, master=None):
        tk.Frame.__init__(self, master)

        self.grid()
        self.pack(side="top", fill="both", expand=True)
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.fig = plt.figure() # animation figure
        self.Create_Widgets()
        self.pack()
        
    def Create_Widgets(self):
        #sliders
        # mass 1
        self.label_Mass1 = tk.Label(self,text="Mass 1").grid(column=1,row=2)
        self.Mass1 = tk.Scale(self, from_=0.01, to=10.00, orient=tk.HORIZONTAL, resolution=0.01)
        self.Mass1.grid(column=1, row = 1)
        self.Mass1.set(1.0)
        # length 1
        self.label_Length1 = tk.Label(self,text="Length 1").grid(column=2,row=2)
        self.Length1 = tk.Scale(self, from_=0.01, to=10.00, orient=tk.HORIZONTAL, resolution=0.01)
        self.Length1.grid(column=2, row = 1)
        self.Length1.set(5.0)
        # theta 1
        self.label_Theta1 = tk.Label(self,text="Angle 1").grid(column=3,row=2)
        self.Theta1 = tk.Scale(self, from_=0.00, to=360.00, orient=tk.HORIZONTAL, resolution=1.0)
        self.Theta1.grid(column=3, row = 1)
        self.Theta1.set(90)
        # angular velocity 1
        self.label_Angvel1 = tk.Label(self,text="Angular Velocity 1").grid(column=4,row=2)
        self.Angvel1 = tk.Scale(self, from_=0.00, to=10.00, orient=tk.HORIZONTAL, resolution=0.01)
        self.Angvel1.grid(column=4, row = 1)
        self.Angvel1.set(0)

        # mass 2
        self.label_Mass2 = tk.Label(self,text="Mass 2").grid(column=1,row=4)
        self.Mass2 = tk.Scale(self, from_=0.01, to=10.00, orient=tk.HORIZONTAL, resolution=0.01)
        self.Mass2.grid(column=1, row = 3)
        self.Mass2.set(1.0)
        # length 2
        self.label_Length2 = tk.Label(self,text="Length 2").grid(column=2,row=4)
        self.Length2 = tk.Scale(self, from_=0.01, to=10.00, orient=tk.HORIZONTAL, resolution=0.01)
        self.Length2.grid(column=2, row = 3)
        self.Length2.set(5.0)
        # theta 2
        self.label_Theta2 = tk.Label(self,text="Angle 2").grid(column=3,row=4)
        self.Theta2 = tk.Scale(self, from_=0.00, to=360.00, orient=tk.HORIZONTAL, resolution=1.0)
        self.Theta2.grid(column=3, row = 3)
        self.Theta2.set(90)
        # angular velocity 2
        self.label_Angvel2 = tk.Label(self,text="Angular Velocity 2").grid(column=4,row=4)
        self.Angvel2 = tk.Scale(self, from_=0.00, to=10.00, orient=tk.HORIZONTAL, resolution=0.01)
        self.Angvel2.grid(column=4, row = 3)
        self.Angvel2.set(0)
        
        # ---------------------------------------------------------------------------------------------------------------
        # buttons
        self.ani_button = tk.Button(self, text="Animate!", command=self.Anim_Plot).grid(column=1,row=7)
        self.plot = tk.Button(self, text="Position Plot", command=self.All_Plot).grid(column=2,row=7)
        # self.quit = tk.Button(self, text="Quit", command=self.quit).grid(column=3,row=7)
        
        # drop down for time plots
        self.Parameter = tk.StringVar()
        self.Parameter.set("Angle 1")
        self.option = tk.OptionMenu(self, self.Parameter, "Angle 1", "Angular Velocity 1", "Angle 2", "Angular Velocity 2")
        self.option.grid(column=5, row=1)
        go = tk.Button(self, text="Plot Parameter", command = self.IndividualPlot).grid(column=5, row=2)

        # ----------------------------------------------------------------------------------------------------------
        # text boxes
        self.label_tinit = tk.Label(self, text="Time = ").grid(column=1,row=5)
        self.label_tinit = tk.Label(self, text="dt = ").grid(column=3,row=5)
                        
        self.time = tk.StringVar()
        entry = tk.Entry(self,textvariable=self.time).grid(column=2,row=5)
        self.time.set("1.0")
        
        self.dt_entry = tk.StringVar()
        entry = tk.Entry(self,textvariable=self.dt_entry).grid(column=4,row=5)
        self.dt_entry.set("0.05")

    # Uses widgets to set initial conditions
    def Initial_Conditions(self):
        self.Pen2.dt = float(self.dt_entry.get())
        tt = float(self.time.get())
        self.Pen2.t = (np.arange(0.0, tt, self.Pen2.dt))
        
        self.Pen2.mass1 = (self.Mass1.get())
        self.Pen2.length1 = (self.Length1.get())
        self.Pen2.theta1 = (self.Theta1.get())
        self.Pen2.angvel1 = (self.Angvel1.get())
        
        self.Pen2.mass2 = (self.Mass2.get())
        self.Pen2.length2 = (self.Length2.get())
        self.Pen2.theta2 = (self.Theta2.get())
        self.Pen2.angvel2 = (self.Angvel2.get())

    # -----------------------------------------------------------------------------------------
    # Creates animation of the pendulum
    def Anim_Plot(self):
        self.fig.clear()

        self.Initial_Conditions()   # setting initial conditions
        self.x1, self.x2, self.y1, self.y2 = self.Pen2.ODE_solver() # running ODE solver

        self.canvas = FigureCanvasTkAgg(self.fig, master=self)
        self.canvas.get_tk_widget().grid(column=1, row= 10, columnspan=10)

        ax = self.fig.add_subplot(111, autoscale_on=False, xlim=(-20, 20), ylim=(-20, 20))
        ax.grid()

        self.line, = ax.plot([], [], 'o-', lw=2)
        self.time_template = 'time = %.1fs'
        self.time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)
        
        self.ani = animation.FuncAnimation(self.fig, self.animate, np.arange(1, len(self.x1)),
                                      interval=25, blit=False, init_func=self.init)
                
        self.canvas.show()
        
    def init(self):
        self.line.set_data([], [])
        self.time_text.set_text('')
        return self.line, self.time_text

    def animate(self,i):
        thisx = [0, self.x1[i], self.x2[i]]
        thisy = [0, self.y1[i], self.y2[i]]
        self.line.set_data(thisx, thisy)
        self.time_text.set_text(self.time_template%(i*self.Pen2.dt))
        return self.line, self.time_text

    # ----------------------------------------------------------------------------------------
    # Plots positions of masses in time
    def All_Plot(self):
        fig2 = plt.figure(figsize = (5,5))
        fig2.clear()

        plt.plot(self.Pen2.t, self.x1, label="x1")
        plt.plot(self.Pen2.t, self.x2, label="x2")
        plt.plot(self.Pen2.t,self.y1, label="y1")
        plt.plot(self.Pen2.t, self.y2, label="y2")

        legend = plt.legend(loc='upper right')

        plt.title("Position vs Time")
        plt.xlabel("Time [s]")
        plt.ylabel("Position")

        self.canvas = FigureCanvasTkAgg(fig2, master=self)
        self.canvas.get_tk_widget().grid(column=11, row= 10, columnspan=10)
        self.canvas.show()
        
    # Plots angles and angular velocities of pendulum links with time
    def IndividualPlot(self):
        fig3 = plt.figure(figsize = (5,5))
        fig3.clear()

        if self.Parameter.get() == "Angle 1":
            plt.plot( self.Pen2.t, self.Pen2.angle1)
            plt.ylabel("Angle 1 [rad]")
            # plt.title("Angle 1 vs Time")
           
        elif self.Parameter.get() == "Angular Velocity 1":
            plt.plot( self.Pen2.t, self.Pen2.angularvelocity1)
            plt.ylabel("Angular Velocity 1 [rad/s]")
            # plt.title("Angular Velocity 1  vs Time")

        elif self.Parameter.get() == "Angle 2":
            plt.plot( self.Pen2.t, self.Pen2.angle2)
            plt.ylabel("Angle 2 [rad/s]")
            # plt.title("Angle 2  vs Time")

        elif self.Parameter.get() == "Angular Velocity 2":
            plt.plot( self.Pen2.t, self.Pen2.angularvelocity2)
            plt.ylabel("Angular Velocity 2 [rad/s]")  
            # plt.title("Angular Velocity 2 vs Time")   

        string = "%s vs Time" % self.Parameter.get()    # Plot title
        plt.title(string)                               # plot title
        plt.xlabel("Time [s]")
        self.canvas = FigureCanvasTkAgg(fig3, master=self)
        self.canvas.get_tk_widget().grid(column=21, row= 10, columnspan=10)
        self.canvas.show()  

#===============================================================================

# running the program

root = tk.Tk()

app = MainMenu(master=root)
app.master.title("Main Menu")
app.mainloop()
root.destroy()